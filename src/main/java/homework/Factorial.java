package homework;

public class Factorial {

    public static long recursive(long number)
    {
        return (number == 1 || number == 0) ? 1 : number * recursive(number-1);
    }


    public static void main(String[] args) {

        System.out.println(recursive(1));

    }

}
