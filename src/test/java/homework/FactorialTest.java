package homework;

import org.junit.*;

import static org.junit.Assert.*;




public class FactorialTest {


    private Factorial factorial;



    // [test][Method_Name][Condition][Should...Result]
    @Test
    public void Test1ExpectedResult1(){
        // Arrange
        long number = 1;
        long expectedResult = 1;

        // Act
        long result = factorial.recursive(number);

        // Assert
        assertEquals(expectedResult,result);
    }


}
