package one;

import org.junit.*;

import static org.junit.Assert.*;

public class CalculatorTest {


    /**
     * F.I.R.S.T principles of testing
     * Fast  - running tests should not take long
     * Isolated/Independent  - Each test should not depend on any other
     * Repeatable      - gives always the same result
     * Self-validating - no manual action
     * thorough  - all happy path, negative cases, edge& corner cases
     *
     */

    private Calculator calculator;

    @Before
    public void setUp() throws Exception {
        System.out.println("Running...");
        calculator = new Calculator();
    }

    @BeforeClass
    public static void beforeClass(){
        System.out.println("Running before class...");
    }

    // [test][Method_Name][Condition][Should...Result]
    @Test
    public void testAddFirst2Second3Returns5(){
        // Arrange
        int firstNumber = 2;
        int secondNumber = 3;
        int expectedResult = 5;

        // Act
        int result = calculator.add(firstNumber,secondNumber);

        // Assert
        assertEquals(expectedResult,result);
    }

    @Test
    public void testDivideFirst8Second4Returns2(){
        // Arrange
        int firstNumber = 8;
        int secondNumber = 4;
        int expectedResult = 2;

        //Act
        int result = calculator.divide(firstNumber,secondNumber);

        //Assert
        assertEquals(expectedResult, result);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("runs after test method");
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("runs after everything...");
    }
}